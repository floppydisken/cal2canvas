Cal2Canvas
==========

# About
Because of inconsistencies in the way our education system delivered information
such as calendar information I decided to create an application that would
solve that discrepency.  
Cal2Canvas aims to be the linker between some subscription link and Canvas. 

# Gotchas
Be advised, in case you're trying to sync with other links than the specific one
provided by UCN you might run into problems. If this happens, please open up an issue
and I will take a look at it.

# How to use
As of right now there's only been developed a windows desktop client for the
application.
To use it you need the following.
A OAuth token to validate yourself.
This token is need in the Token field in the application.  

The following describes how to get ahold of the subscriptions link.  
Go to timeedit and find you calendar. 
1. ![](images/Timeedit1.PNG)
2. ![](images/Timeedit2.PNG)

The link overwritten by blue text is the one you want.

# Developers
If you wanna contribute or check out the code here's the general overview of the solution.

## Core project
All of the business logic resides here. This is where the .ics provider is 
contacted as well as the Canvas API is contacted. So in short this is where the
merge and persistence lives.
It is targeting .NET Standard, which means it's cross-platform in the sense that
the library can be used by .NET Core as well as .NET Framework.

## MSI and Bootstrapper projects
The windows installer things reside here. This installer is built using the WiX
toolset. For more information on how this framework works checkout https://wixtoolset.org/

## SystemTray project
The Windows GUI built using WinForms for ease of access to the SystemTray functionality.


# Missing
* No indication for the GUI how far the merge is
