﻿using Ical.Net;
using IcalToCanvas.Core.Data;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


[assembly: InternalsVisibleTo("IcalToCanvas.Test")]
namespace IcalToCanvas.Core
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class IcalToCanvasService
    {
        private readonly OAuth2Token AccessToken;
        private readonly Subscription Subscription;

        private readonly bool verbose;
         
        // ISO 8601 formatted date string
        private const string DateTimeFormat = "yyyy-MM-ddTHH\\:mm\\:ssZ";

        private const string BaseUrl = "https://ucn.instructure.com";

        public IcalToCanvasService(Subscription subscription, OAuth2Token token, bool verbose = false)
        {
            Subscription = subscription;
            AccessToken = token;
            this.verbose = verbose;
        }

        private void WriteLine(string message)
        {
            if (verbose)
                Console.WriteLine(message);
        }

        /// <summary>
        /// Merge ical events into Canvas.
        /// </summary>
        /// <param name="fromUrl">.ics file location</param>
        /// <returns></returns>
        public async void MergeIcalIntoCanvas()
        {
            using (var client = SetupHttpClient())
            {
                using (var response = await client.GetAsync(Subscription.Uri))
                {
                    var intermediateCalendar = new IntermediateCalendar(Calendar.Load(await response.Content.ReadAsStringAsync()));

                    List<IntermediateEvent> currentCanvasEvents = await FetchAllEventsFromCanvas();

                    List<IntermediateEvent> filteredEvents = 
                        FilterEvents(currentCanvasEvents, intermediateCalendar.Events);

                    // Print out for debugging.
                    filteredEvents.ForEach(e => WriteLine($"Missing: {e.Description}, Start: {e.Start}, End: {e.End}"));

                    //Upload the events to Canvas
                    foreach (IntermediateEvent ev in filteredEvents)
                    {
                        // Setup request
                        var httpContent = new FormUrlEncodedContent(await GenerateEventContent(ev));
                        var request = new HttpRequestMessage(HttpMethod.Post, GenerateEventUrl())
                        {
                            Content = new FormUrlEncodedContent(await GenerateEventContent(ev))
                        };

                        // OAuth Token. Can be dynamically acquired with dev key from admins.
                        HttpResponseMessage postReply = await client.SendAsync(request);
                        PrintHttpResponse(HttpMethod.Post, postReply);
                    }
                }
            }
        }

        private void PrintHttpResponse(HttpMethod method, HttpResponseMessage msg)
        {
            WriteLine("--------------");
            WriteLine(
                $"{method} {(msg.IsSuccessStatusCode ? "successful" : "failed")}"
                + $", code = {msg.StatusCode}"
                + $", url = {GenerateEventUrl()}"
            );

        }

        public void ResetCanvasCalendar()
        {
            ClearCanvasCalendar();
            MergeIcalIntoCanvas();
        }

        public async void ClearCanvasCalendar()
        {
            List<IntermediateEvent> events = await FetchAllEventsFromCanvas();

            using (var client = SetupHttpClient())
            {
                foreach (var ev in events)
                {
                    var request = new HttpRequestMessage(HttpMethod.Delete, $"{GenerateEventUrl()}/{ev.Id}");
                    PrintHttpResponse(HttpMethod.Delete, await client.SendAsync(request));
                }
            }
        }

        internal async Task<CanvasUser> FetchCanvasUser()
        {
            CanvasUser user;

            using (var client = SetupHttpClient())
            {
                var request  = new HttpRequestMessage(HttpMethod.Get, $"{BaseUrl}/api/v1/users/self");
                var response = await client.SendAsync(request);
                var contentAsJson   = JToken.Parse(await response.Content.ReadAsStringAsync());

                user = new CanvasUser(contentAsJson);
            }

            return user;
        }

        private async Task<CanvasResponse> InitialCanvasRequest(HttpClient httpClient, CanvasUser canvasUser, int pageSize = 50)
        {
            var canvasRequest = new HttpRequestMessage(HttpMethod.Get
                , $"{GenerateEventUrl()}?context_codes[]={canvasUser.IdAsString}&all_events=true&per_page={pageSize}");
            HttpResponseMessage canvasResult = await httpClient.SendAsync(canvasRequest);

            return await CanvasResponse.Build(canvasResult);
        }

        private async Task<CanvasResponse> SubsequentCanvasRequest(HttpClient httpClient, CanvasResponse canvasResponse, int pageSize = 50)
        {
            var canvasRequest = new HttpRequestMessage(HttpMethod.Get
                , $"{canvasResponse.NextUri}&per_page={pageSize}");
            var canvasResult = await httpClient.SendAsync(canvasRequest);
            
            return await CanvasResponse.Build(canvasResult);
        }

        /// <summary>
        /// Retrieves all calendar events without limit
        /// </summary>
        /// <returns>List of events asynchronously</returns>
        internal async Task<List<IntermediateEvent>> FetchAllEventsFromCanvas(int pageSize = 50)
        {
            List<IntermediateEvent> allIntermediateEvents = new List<IntermediateEvent>();
            CanvasResponse canvasResponse = null;

            // TODO Consider either making this recursive or refactoring some functions out.
            using (HttpClient client = SetupHttpClient())
            {
                CanvasUser canvasUser = await FetchCanvasUser();
                canvasResponse = await InitialCanvasRequest(client, canvasUser);
                allIntermediateEvents.AddRange(canvasResponse.Events);

                // Keep calling the Canvas API until we've reached the end of the paging
                while (canvasResponse.HasNextUri)
                {
                    // Should only be 1 iteration.
                    canvasResponse = await SubsequentCanvasRequest(client, canvasResponse);
                    allIntermediateEvents.AddRange(canvasResponse.Events);
                }
            }

            return allIntermediateEvents;
        }

        /// <summary>
        /// Very dumb implementation of a link header parser.
        /// If extra parameters emerges for the link header, the implementation will most likely fail. 
        /// The links come in this format, and the method aims to extract the next page of the events.
        /// Link: <https://<canvas>/api/v1/courses/:id/discussion_topics.json?opaqueA>; rel="current",
        ///       <https://<canvas>/api/v1/courses/:id/discussion_topics.json?opaqueB>; rel="next",
        ///       <https://<canvas>/api/v1/courses/:id/discussion_topics.json?opaqueC>; rel="first",
        ///       <https://<canvas>/api/v1/courses/:id/discussion_topics.json?opaqueD>; rel="last"
        /// </summary>
        /// <param name="linkHeader">The link header from the Response</param>
        /// <returns>
        /// Returns a with the above extracted values Dictionary<extractedRel, extractedUri>
        /// An example of a key value pair would be
        /// "next" : "https://<canvas>/api/v1/courses/:id/discussion_topics.json?opaqueA"
        /// </returns>
        internal Dictionary<string, Uri> ExtractPaginationUris(string linkHeader)
        {
            var result = new Dictionary<string, Uri>();
            char globalNextValueDelimiter = ',';
            char localNextValueDelimiter = ';';

            string[] links = linkHeader.Split(globalNextValueDelimiter);

            foreach (string pair in links)
            {
                string[] splitPair = pair.Split(localNextValueDelimiter);
                string link = splitPair[0];
                string rel = splitPair[1];

                // We don't want <> which are the first and last characters.
                link = link.Trim().Substring(1, link.Length - 2);

                string[] splitRel = rel.Split('=');
                string relName = splitRel[0];
                string relVal = splitRel[1];
                relVal = Regex.Replace(relVal, "\"", "");

                WriteLine($"Link    : {link}");
                WriteLine($"RelValue: {relVal}");

                result.Add(relVal, new Uri(link));
            }

            return result;
        }

        private HttpClient SetupHttpClient()
        {
            var client = new HttpClient()
            {
                DefaultRequestHeaders =
                {
                    Authorization = new AuthenticationHeaderValue("Bearer", AccessToken.ToString())
                }
            };

            return client;
        }

        /// <summary>
        /// Fetches the current events a user has, and filters the list to be uploaded by those values.
        /// </summary>
        /// <param name="filterBy">Events we don't want to upload to canvas</param>
        /// <param name="eventsToFilter">List to filter</param>
        internal List<IntermediateEvent> FilterEvents(List<IntermediateEvent> filterBy, List<IntermediateEvent> eventsToFilter)
        {
            var result = new List<IntermediateEvent>(eventsToFilter);

            // Simple filtering. We're basing it on the descriptions of the events, as they hold the Id atm.
            // TODO create a ID element for C2CEvents, so it's possible to compare without using misleading properties. Description should not be the identifier
            foreach (IntermediateEvent intermediateEvent in eventsToFilter)
                foreach (IntermediateEvent intermediateEventToMatch in filterBy)
                    if (intermediateEvent.Description == intermediateEventToMatch.Description)
                        result.Remove(intermediateEvent);

            return result;
        }

        internal string GenerateEventUrl()
        {
            var path        = "/api/v1/calendar_events";

            Uri url = new Uri($"{BaseUrl}{path}");
            return url.AbsoluteUri;
        }

        internal async Task<Dictionary<string, string>> GenerateEventContent(IntermediateEvent intermediateEvent)
        {
            CanvasUser canvasUser = await FetchCanvasUser();

            var content = new Dictionary<string, string>()
            {
                { "calendar_event[context_code]"    , canvasUser.IdAsString },
                { "calendar_event[title]"           , intermediateEvent.Summary },
                { "calendar_event[start_at]"        , intermediateEvent.Start.ToUniversalTime().ToString(DateTimeFormat) },
                { "calendar_event[end_at]"          , intermediateEvent.End.ToUniversalTime().ToString(DateTimeFormat) },
                { "calendar_event[description]"     , intermediateEvent.Description },
                { "calendar_event[location_name]"   , intermediateEvent.Location },
            };

            return content;
        }
    }
}
