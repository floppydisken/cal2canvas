﻿using Ical.Net;
using Ical.Net.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Ical.Net.CalendarComponents;
using Newtonsoft.Json.Linq;

namespace IcalToCanvas.Core.Data
{
    public class IntermediateCalendar
    {
        public List<IntermediateEvent> Events { get; set; } = new List<IntermediateEvent>();

        public IntermediateCalendar(Calendar calendar)
        {
            foreach (CalendarEvent calendarEvent in calendar.Events)
            {
                Events.Add(new IntermediateEvent(calendarEvent));
            }
        }

        public IntermediateCalendar(JArray calendarEventsAsJson)
        {
            foreach (JToken calendarEvent in calendarEventsAsJson) {
                Events.Add(new IntermediateEvent(calendarEvent));
            }
        }

    }
}
