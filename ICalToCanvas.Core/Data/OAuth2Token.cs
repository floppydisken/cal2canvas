﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IcalToCanvas.Core.Data
{
    public class OAuth2Token
    {
        public string Value { get; set; }

        public OAuth2Token(string token)
        {
            Value = token;
        }

        public override string ToString()
        {
            return Value;
        }
    }
}
