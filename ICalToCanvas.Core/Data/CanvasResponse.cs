﻿using IcalToCanvas.Core.Data;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IcalToCanvas.Core.Data
{
    public class CanvasResponse
    {
        public Dictionary<string, Uri> LinkHeader = new Dictionary<string, Uri>();
        public List<IntermediateEvent> Events = new List<IntermediateEvent>();

        private const string NextName = "next";


        private CanvasResponse() { }

        private CanvasResponse(Dictionary<string, Uri> linkHeader, JArray content)
        {
            LinkHeader = linkHeader;

            foreach (JToken jsonToken in content)
            {
                Events.Add(new IntermediateEvent(jsonToken));
            }
        }

        public static async Task<CanvasResponse> Build(HttpResponseMessage response)
        {
            var canvasResponse = new CanvasResponse();

            canvasResponse.LinkHeader = GetFirstLinkHeader(response);

            foreach (JToken eventAsJson in JArray.Parse(await response.Content.ReadAsStringAsync()))
            {
                canvasResponse.Events.Add(new IntermediateEvent(eventAsJson));
            }

            return canvasResponse;
        }

        private static Dictionary<string, Uri> GetFirstLinkHeader(HttpResponseMessage response)
        {
            IEnumerator<string> linkHeaderEnumerator =
                response.Headers.GetValues("Link").GetEnumerator();

            // If not empty
            if (linkHeaderEnumerator.MoveNext())
                return ExtractPaginationUris(linkHeaderEnumerator.Current);

            // If empty
            return new Dictionary<string, Uri>();
        }

        public bool ContainsUri(string relName) => LinkHeader.ContainsKey(relName);
        public Uri  NextUri      => LinkHeader[NextName];
        public bool HasNextUri   => ContainsUri(NextName);

        /// <summary>
        /// Very dumb implementation of a link header parser.
        /// If extra parameters emerges for the link header, the implementation will most likely fail. 
        /// The links come in this format, and the method aims to extract the next page of the events.
        /// Link: <https://<canvas>/api/v1/courses/:id/discussion_topics.json?opaqueA>; rel="current",
        ///       <https://<canvas>/api/v1/courses/:id/discussion_topics.json?opaqueB>; rel="next",
        ///       <https://<canvas>/api/v1/courses/:id/discussion_topics.json?opaqueC>; rel="first",
        ///       <https://<canvas>/api/v1/courses/:id/discussion_topics.json?opaqueD>; rel="last"
        /// </summary>
        /// <param name="linkHeader">The link header from the Response</param>
        /// <returns>
        /// Returns a with the above extracted values Dictionary<extractedRel, extractedUri>
        /// An example of a key value pair would be
        /// "next" : "https://<canvas>/api/v1/courses/:id/discussion_topics.json?opaqueA"
        /// </returns>
        private static Dictionary<string, Uri> ExtractPaginationUris(string linkHeader)
        {
            var result = new Dictionary<string, Uri>();
            char globalNextValueDelimiter = ',';
            char localNextValueDelimiter = ';';

            string[] links = linkHeader.Split(globalNextValueDelimiter);

            foreach (string pair in links)
            {
                string[] splitPair = pair.Split(localNextValueDelimiter);
                string link = splitPair[0];
                string rel = splitPair[1];

                // We don't want <> which are the first and last characters.
                link = link.Trim().Substring(1, link.Length - 2);

                string[] splitRel = rel.Split('=');
                string relName = splitRel[0];
                string relVal = splitRel[1];
                relVal = Regex.Replace(relVal, "\"", "");

                Debug.WriteLine($"Link    : {link}");
                Debug.WriteLine($"RelValue: {relVal}");

                result.Add(relVal, new Uri(link));
            }

            return result;
        }


    }
}
