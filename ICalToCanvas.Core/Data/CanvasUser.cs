﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace IcalToCanvas.Core.Data
{
    class CanvasUser
    {
        public long Id { get; set; }

        /// <summary>
        /// Describes the Id as canvas wants it when considering context
        /// </summary>
        public string IdAsString => $"user_{Id}";

        /// <summary>
        /// Construct the object based on the Canvas JSON response.
        /// </summary>
        /// <param name="userData">The root of the json object returned by canvas</param>
        public CanvasUser(JToken userData)
        {
            Id = (long)userData["id"];
        }
    }
}
