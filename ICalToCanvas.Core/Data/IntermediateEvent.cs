﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Ical.Net;
using Ical.Net.CalendarComponents;
using Newtonsoft.Json.Linq;

namespace IcalToCanvas.Core.Data
{
    public class IntermediateEvent
    {
        // Only used when parsed from Canvas instead of .ics
        internal long Id { get; set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Description { get; set; }
        public string Summary { get; set; }
        public string Location { get; set; }
        
        internal IntermediateEvent() { }

        public IntermediateEvent(CalendarEvent cEvent)
        {
            Start       = cEvent.DtStart.AsSystemLocal;
            End         = cEvent.DtEnd.AsSystemLocal;
            Description = cEvent.Description;
            Location    = cEvent.Location;
            Summary     = cEvent.Summary;
        }

        public IntermediateEvent(JToken jsonEvent)
        {
            Id          = (long)     jsonEvent["id"];
            Start       = (DateTime) jsonEvent["start_at"];
            End         = (DateTime) jsonEvent["end_at"];
            Description = (string)   jsonEvent["description"];
            Summary     = (string)   jsonEvent["summary"];
            Location    = (string)   jsonEvent["location"];
        }
    }
}
