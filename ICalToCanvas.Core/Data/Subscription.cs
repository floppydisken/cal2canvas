﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IcalToCanvas.Core.Data
{
    public class Subscription
    {
        public Uri Uri { get; set; }

        public Subscription(Uri uri)
        {
            Uri = uri;
        }

        public Subscription(string uri)
        {
            Uri = new Uri(uri);
        }
    }
}
