﻿using CommandLine;
using IcalToCanvas.Core;
using IcalToCanvas.Core.Data;
using System;


namespace IcalToCanvas.Cli
{
    class Program
    {
        public class Options
        {
            [Option('v', "verbose", Required=false, HelpText="Set output to verbose messages.")]
            public bool Verbose { get; set; }

            [Option('a', "canvas-apikey", Required = true, HelpText = "Canvas Api key.")]
            public string CanvasApiKey { get; set; }

            [Option('c', "calendar-subscription", Required = true, HelpText = "The ical/.ics file subscribed to")]
            public string CalendarSubscription { get; set; }
        }

        static void Main(string[] args)
        {
            CommandLine.Parser.Default.ParseArguments<Options>(args)
                .WithParsed<Options>(options => {
                    Console.WriteLine($"Is verbose {options.Verbose}");
                    var service = new IcalToCanvasService(
                          new Subscription(options.CalendarSubscription)
                        , new OAuth2Token(options.CanvasApiKey)
                        , true);
                    service.ClearCanvasCalendar();
                    Console.WriteLine("Cleared Canvas calendar");

                    service.MergeIcalIntoCanvas();
                    Console.WriteLine("Merged Canvas calendar");
                });
        }
    }
}
