﻿using System;
using System.Collections.Generic;
using Ical.Net;
using IcalToCanvas.Core.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IcalToCanvas.Test
{
    [TestClass]
    public class ModelTests
    {
        // This is how the data will come in
        const string events = @"
BEGIN:VCALENDAR
VERSION:2.0
METHOD:PUBLISH
X-WR-CALNAME:TimeEdit-Bjarke Mac Sporring-20190201
X-WR-CALDESC:Date limit 2019-02-04 - 2024-02-25
X-PUBLISHED-TTL:PT20M
CALSCALE:GREGORIAN
PRODID:-//TimeEdit\\\, //TimeEdit//EN
BEGIN:VEVENT
DTSTART:20190219T073000Z
DTEND:20190219T090000Z
UID:438508--425808234-0@timeedit.com
DTSTAMP:20190220T074910Z
LAST-MODIFIED:20190220T074910Z
SUMMARY:dmaa0218\, Systemudvikling\, Karsten Jeppesen
LOCATION:CSD 3.2.17
DESCRIPTION:Id 438508
END:VEVENT
BEGIN:VEVENT
DTSTART:20190219T091500Z
DTEND:20190219T104500Z
UID:438509--425808234-0@timeedit.com
DTSTAMP:20190220T074910Z
LAST-MODIFIED:20190220T074910Z
SUMMARY:dmaa0218\, Systemudvikling\, Karsten Jeppesen
LOCATION:CSD 3.2.17
DESCRIPTION:Id 438509
END:VEVENT
END:VCALENDAR";

        [TestMethod]
        public void WhenicalCalender_MapCorrectAmountOfEvents()
        {
            var cal = Calendar.Load(events);
            var c2cCal = new IntermediateCalendar(cal);

            // Parsed two event
            Assert.IsTrue(2 == c2cCal.Events.Count);
        }

        [TestMethod]
        public void WhenicalCalender_MapEventsCorrectly()
        {
            var cal = Calendar.Load(events);
            List<IntermediateEvent> c2cEvents = new IntermediateCalendar(cal).Events;
            var ev1 = c2cEvents[0];
            var ev2 = c2cEvents[1];

            // Tedious value checks
            Assert.AreEqual(0, ev1.Start.CompareTo(DateTime.Parse("2019-02-19T07:30:00Z")));
            Assert.AreEqual(0, ev1.End.CompareTo(DateTime.Parse("2019-02-19T09:00:00Z")));
            Assert.AreEqual("Id 438508", ev1.Description);
            Assert.AreEqual("CSD 3.2.17", ev1.Location);

            Assert.AreEqual(0, ev2.Start.CompareTo(DateTime.Parse("2019-02-19T09:15:00Z")));
            Assert.AreEqual(0, ev2.End.CompareTo(DateTime.Parse("2019-02-19T10:45:00Z")));
            Assert.AreEqual("Id 438509", ev2.Description);
            Assert.AreEqual("CSD 3.2.17", ev2.Location);
        }
    }
}
