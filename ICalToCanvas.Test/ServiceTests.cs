using System;
using System.Collections.Generic;
using System.Linq;
using Ical.Net;
using IcalToCanvas.Core;
using IcalToCanvas.Core.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;

namespace IcalToCanvas.Test
{
    [TestClass]
    public class ServiceTests
    {
        // This is how the data will come in
        const string testEvents = @"
BEGIN:VCALENDAR
VERSION:2.0
METHOD:PUBLISH
X-WR-CALNAME:TimeEdit-Bjarke Mac Sporring-20190201
X-WR-CALDESC:Date limit 2019-02-04 - 2024-02-25
X-PUBLISHED-TTL:PT20M
CALSCALE:GREGORIAN
PRODID:-//TimeEdit\\\, //TimeEdit//EN
BEGIN:VEVENT
DTSTART:20190219T073000Z
DTEND:20190219T090000Z
UID:438508--425808234-0@timeedit.com
DTSTAMP:20190220T074910Z
LAST-MODIFIED:20190220T074910Z
SUMMARY:dmaa0218\, Systemudvikling\, Karsten Jeppesen
LOCATION:CSD 3.2.17
DESCRIPTION:Id 438508
END:VEVENT
BEGIN:VEVENT
DTSTART:20190219T091500Z
DTEND:20190219T104500Z
UID:438509--425808234-0@timeedit.com
DTSTAMP:20190220T074910Z
LAST-MODIFIED:20190220T074910Z
SUMMARY:dmaa0218\, Systemudvikling\, Karsten Jeppesen
LOCATION:CSD 3.2.17
DESCRIPTION:Id 438509
END:VEVENT
END:VCALENDAR";


        private const string dateTimeFormat = "yyyy-MM-ddTHH\\:mm\\:ssZ";

        public List<IntermediateEvent> events { get; set; }
        [TestInitialize]
        public void init()
        {
            var cal = Calendar.Load(testEvents);
            events = new IntermediateCalendar(cal).Events;
        }

        [TestMethod]
        public void WhenC2CCalMerge_GenerateCorrectUrl()
        {
            var service = new C2CService();
            string url = service.GenerateEventUrl();

            var expected = $"https://ucn.instructure.com/api/v1/calendar_events";

            Assert.AreEqual(expected, url);
        }

        [TestMethod]
        public async void WhenC2CalMerge_GenerateCorrectHttpContent()
        {
            var service = new C2CService();
            var c2cEvent = new IntermediateEvent()
            {
                Start = DateTime.Now,
                End = DateTime.Now.AddDays(2),
                Description = "Id 1234",
                Summary = "Some class somewhere",
                Location = "CSD 1337",
            };

            var expectedContent = new Dictionary<string, string>()
            {
                { "calendar_event[context_code]",   "user_39175" },
                { "calendar_event[title]",          c2cEvent.Summary },
                { "calendar_event[start_at]",       c2cEvent.Start.ToString(dateTimeFormat) },
                { "calendar_event[end_at]",         c2cEvent.End.ToString(dateTimeFormat) },
                { "calendar_event[description]",    c2cEvent.Description },
                { "calendar_event[location_name]",  c2cEvent.Location },
            };

            Assert.IsTrue(DictsAreEqual(expectedContent, await service.GenerateEventContent(c2cEvent)));
        }

        /// <summary>
        /// Compares the dict keys and values in no particular order.
        /// </summary>
        /// <param name="dict1">left hand side value</param>
        /// <param name="dict2">right hand side value</param>
        /// <returns></returns>
        private bool DictsAreEqual(Dictionary<string, string> dict1, Dictionary<string, string> dict2)
        {
            if (!dict1.Count.Equals(dict2.Count)) return false;

            bool isEqual = true;

            var keyIt = dict1.Keys.GetEnumerator();
            while (keyIt.MoveNext() && isEqual)
                if (!dict2.ContainsKey(keyIt.Current))
                    isEqual = false;

            var valIt = dict1.Values.GetEnumerator();
            while (valIt.MoveNext() && isEqual)
                if (!dict2.ContainsValue(valIt.Current))
                    isEqual = false;
            

            return isEqual;
        }

        [TestMethod]
        public void WhenFilteringC2CEvents_ReturnCorrectlyFilteredList()
        {
            var ev1 = new IntermediateEvent()
            {
                Start = DateTime.Now,
                End = DateTime.Now.AddDays(2),
                Description = "Id 1234",
                Summary = "Some class somewhere",
                Location = "CSD 1337",
            };

            var ev2 = new IntermediateEvent()
            {
                Start = DateTime.Now.AddDays(3),
                End = DateTime.Now.AddDays(5),
                Description = "Id 1235",
                Summary = "Some class somewhere",
                Location = "CSD 1337",
            };

            var ev3 = new IntermediateEvent()
            {
                Start = DateTime.Now.AddDays(6),
                End = DateTime.Now.AddDays(6),
                Description = "Id 1236",
                Summary = "Some class somewhere",
                Location = "CSD 1337",
            };

            var events = new List<IntermediateEvent>() { ev1, ev2, ev3, };

            var filterBy = new List<IntermediateEvent>() { ev1, ev2 };

            var service = new C2CService();

            var result = service.FilterEventsBy(filterBy, events);

            Assert.IsTrue(result.Contains(ev3));
            Assert.IsFalse(result.Contains(ev1));
            Assert.IsFalse(result.Contains(ev2));
        }

        [TestMethod]
        public void WhenParsingLinksHeader_ParseCorrectly()
        {
            // TODO Consider simplyfying the data.
            string testData =
                "<https://ucn.beta.instructure.com/api/v1/calendar_events?all_events=true&context_codes%5B%5D=user_39175&page=4&per_page=10>; rel=\"current\"" +
                ",<https://ucn.beta.instructure.com/api/v1/calendar_events?all_events=true&context_codes%5B%5D=user_39175&page=5&per_page=10>; rel=\"next\"" +
                ",<https://ucn.beta.instructure.com/api/v1/calendar_events?all_events=true&context_codes%5B%5D=user_39175&page=1&per_page=10>; rel=\"first\"" +
                ",<https://ucn.beta.instructure.com/api/v1/calendar_events?all_events=true&context_codes%5B%5D=user_39175&page=12&per_page=10>; rel=\"last\"";

            var service = new C2CService();
            var current =
                "https://ucn.beta.instructure.com/api/v1/calendar_events?all_events=true&context_codes%5B%5D=user_39175&page=4&per_page=10";
            var next =
                "https://ucn.beta.instructure.com/api/v1/calendar_events?all_events=true&context_codes%5B%5D=user_39175&page=5&per_page=10";
            var first =
                "https://ucn.beta.instructure.com/api/v1/calendar_events?all_events=true&context_codes%5B%5D=user_39175&page=1&per_page=10";
            var last =
                "https://ucn.beta.instructure.com/api/v1/calendar_events?all_events=true&context_codes%5B%5D=user_39175&page=12&per_page=10";

            Dictionary<string, string> result = service.ParseLinks(testData);

            Assert.IsTrue(result.Keys.Contains("current"));
            Assert.IsTrue(result.Keys.Contains("next"));
            Assert.IsTrue(result.Keys.Contains("first"));
            Assert.IsTrue(result.Keys.Contains("last"));

            Assert.IsTrue(result.Values.Contains(current));
            Assert.IsTrue(result.Values.Contains(next));
            Assert.IsTrue(result.Values.Contains(first));
            Assert.IsTrue(result.Values.Contains(last));

            Assert.IsTrue(result["current"] == current);
            Assert.IsTrue(result["next"] == next);
            Assert.IsTrue(result["first"] == first);
            Assert.IsTrue(result["last"] == last);
        }

        [TestMethod]
        public void GivenJTokenHasBeenFetch_WhenConvertingToPOCO_ThenDoItCorrectly()
        {
            string json = "{\"id\": 39175}";
            
            var user = new CanvasUser(JToken.Parse(json));

            Assert.IsTrue(user.Id == 39175);
            Assert.IsTrue(user.IdAsString == "user_39175");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GivenJTokenIsFetchedButWrongFormat_WhenConvertingToPOCO_ThenThrowException()
        {
            string json = "{\"1\" : {\"id\": 1234 }}";
            
            var user = new CanvasUser(JToken.Parse(json));

            Assert.IsTrue(user.Id == 39175);
            Assert.IsTrue(user.IdAsString == "user_39175");
        }
    }
}
