﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ICalToCanvas.DataAccess.Factory;
using ICalToCanvas.DataAccess.Data;
using System.Collections.Generic;

namespace ICalToCanvas.Test
{
    [TestClass]
    public class FactoryTests
    {

        [TestMethod]
        public void WhenTypeICalendarEvent_CreateCalendarEventFactoryAsEntityFactory()
        {
            var factory = EntityFactory<ICalendarEvent>.Init();

            Assert.IsTrue(factory is CalendarEventFactory);
        }

        [TestMethod]
        public void WhenParsingCalendarEvents_ReturnListOfCorrectElements()
        {
            // This is how the data will come in
            const string events = @"
BEGIN:VCALENDAR
VERSION:2.0
METHOD:PUBLISH
X-WR-CALNAME:TimeEdit-Bjarke Mac Sporring-20190201
X-WR-CALDESC:Date limit 2019-02-04 - 2024-02-25
X-PUBLISHED-TTL:PT20M
CALSCALE:GREGORIAN
PRODID:-//TimeEdit\\\, //TimeEdit//EN
BEGIN:VEVENT
DTSTART:20190219T073000Z
DTEND:20190219T090000Z
UID:438508--425808234-0@timeedit.com
DTSTAMP:20190220T074910Z
LAST-MODIFIED:20190220T074910Z
SUMMARY:dmaa0218\, Systemudvikling\, Karsten Jeppesen
LOCATION:CSD 3.2.17
DESCRIPTION:Id 438508
END:VEVENT
BEGIN:VEVENT
DTSTART:20190219T091500Z
DTEND:20190219T104500Z
UID:438509--425808234-0@timeedit.com
DTSTAMP:20190220T074910Z
LAST-MODIFIED:20190220T074910Z
SUMMARY:dmaa0218\, Systemudvikling\, Karsten Jeppesen
LOCATION:CSD 3.2.17
DESCRIPTION:Id 438509
END:VEVENT
END:VCALENDAR";

            var factory = EntityFactory<ICalendarEvent>.Init();

            List<ICalendarEvent> calendarEvents = factory.ParseICalFile(events);

            ICalendarEvent event1 = calendarEvents[0];
            ICalendarEvent event2 = calendarEvents[1];

            // We simply test the start and end dates. This can be expanded upon ofc.
            Assert.AreEqual(event1.Start, DateTime.Parse("20190219T073000Z"));
            Assert.AreEqual(event1.End, DateTime.Parse("20190219T090000Z"));

            Assert.AreEqual(event2.Start, DateTime.Parse("20190219T091500Z"));
            Assert.AreEqual(event2.End, DateTime.Parse("20190219T104500Z"));
        }
    }
}
